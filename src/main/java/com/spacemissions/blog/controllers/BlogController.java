package com.spacemissions.blog.controllers;

import com.spacemissions.blog.models.Mission;
import com.spacemissions.blog.repo.MissionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@Controller
@RequestMapping(value = "/blog")
public class BlogController {

    @Autowired
    private MissionsRepository missionsRepository;

    @GetMapping()
    public String blogMain(Model model) {
        Iterable<Mission> missions = missionsRepository.findAll();
        model.addAttribute("missions", missions);
        return "blog";
    }

    @GetMapping("/add")
    public String blogAdd(Model model) {
        return "mission-add";
    }

    @PostMapping("/add")
    public String blogPostAdd(@RequestParam String title, @RequestParam String anons, @RequestParam String fullText, Model model) {
        Mission mission = new Mission(title, anons, fullText);
        missionsRepository.save(mission);
        return "redirect:/blog";
    }

    @GetMapping("/{id}")
    public String blogDetails(@PathVariable(value ="id") long id, Model model) {
        if(!missionsRepository.existsById(id)) {
            return "redirect:/blog";
        }

        Optional<Mission> mission = missionsRepository.findById(id);
        ArrayList<Mission> res = new ArrayList<>();
        mission.ifPresent(res::add);
        model.addAttribute("mission", res);
        return "mission-details";
    }

    @GetMapping("/{id}/edit")
    public String blogEdit(@PathVariable(value ="id") long id, Model model) {
        if(!missionsRepository.existsById(id)) {
            return "redirect:/blog";
        }

        Optional<Mission> mission = missionsRepository.findById(id);
        ArrayList<Mission> res = new ArrayList<>();
        mission.ifPresent(res::add);
        model.addAttribute("mission", res);
        return "mission-edit";
    }

    @PostMapping("/{id}/edit")
    public String blogPostUpdate(@PathVariable(value ="id") long id, @RequestParam String title, @RequestParam String anons, @RequestParam String fullText, Model model) {
        Mission mission = missionsRepository.findById(id).orElseThrow(IllegalStateException::new);
        mission.setTitle(title);
        mission.setAnons(anons);
        mission.setFullText(fullText);
        missionsRepository.save(mission);

        return "redirect:/blog";
    }

    @PostMapping("/{id}/remove")
    public String blogPostRemove(@PathVariable(value ="id") long id, Model model) {
        Mission mission = missionsRepository.findById(id).orElseThrow(IllegalStateException::new);
        missionsRepository.delete(mission);
        return "redirect:/blog";
    }


}
