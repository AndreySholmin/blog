package com.spacemissions.blog.repo;

import com.spacemissions.blog.models.Mission;
import org.springframework.data.repository.CrudRepository;

public interface MissionsRepository extends CrudRepository<Mission, Long> {}
